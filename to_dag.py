
import inspect
import os, sys
import re
from collections import defaultdict
import itertools
import string
import random
random.seed(30)
import collections
import matplotlib.pyplot as plt

import networkx as nx

# from genpipes.pipelines.chipseq import ChipSeq
# from genppes.pipelines.chipseq import ChipSeq
from genpipes.pipelines import RnaSeq
import genpipes.bfx.readset

from genpipes.pipelines import DnaSeq

from genpipes.core import Pipeline

class Task(object):

    READSET_INPUTS = {'bam': 'X0X_bam_X0X',
                      'adapter1': 'X0X_adapter1_X0X',
                      'adapter2': 'X0X_adapter2_X0X'}
    READSET_INPUTS = collections.namedtuple('Args', ' '.join(list(READSET_INPUTS.keys())))(**READSET_INPUTS)
    SCATTER = 'scatter'
    SAMPLE = 'X0X_sample_X0X'
    READSET = 'X0X_readset_X0X'
    INI_PARAM = 'ini_param'
    OTHER_STAMP = 'others'
    COMMAND = 'command'
    INPUT_FILE = 'input_file'
    OUTPUT_FILE = 'output_file'

    GATHER = 'gather'
    OVER_READSET = '0X0_over_readset_0X0'
    OVER_SAMPLE = '0X0_over_sample_0X0'

    def __init__(self, name):
        """ A task can be :
                    one to one (readset set to readset of sample to sample)
                    many to one (readsets to sample or samples to unit)
                    one to many (unit to readsets, unit to sample or sample to readsets)

        :param name: Task name
        """
        self.name = name
        self._input_files = None
        self._output_files = None
        self._command = None
        self.gen_jobs = []
        self.input_type = None
        self.output_type = None
        self.command_type = None


    def __str__(self):

        return '{}'.format(self.name)



    @property
    def input_files(self):
        return self._input_files

    @input_files.setter
    def input_files(self, value):

        if self._input_files is not None:
            if self._input_files != value:
                print self._input_files
                print value
                raise ValueError
        else:
            self._input_files = value

    @property
    def output_files(self):
        return self._output_files
    
    @output_files.setter
    def output_files(self, value):

        if self._output_files is not None:
            if self._output_files != value:
                raise ValueError
        else:
            self._output_files = value

    @property
    def command(self):
        return self._command
    
    @command.setter
    def command(self, value):
        if self._command is not None:
            if self._command != value:
                raise ValueError
        else:
            self._command = value

class Workflow(object):


    DECOR = 'X0X0'
    ALL_SAMPLE = "{0}_ALL_SAMPLE_{0}".format(DECOR)
    ALL_SAMPLE_READSET = "{0}_ALL_SAMPLE_READSET_{0}".format(DECOR)
    ALL_READSET = "{0}_ALL_READSET_{0}".format(DECOR)
    SAME_SAMPLE = "{0}_SAME_SAMPLE_{0}".format(DECOR)
    SAME_READSET = "{0}_SAME_READSET_{0}".format(DECOR)


    def __init__(self, name, genpipes_wf, readset_in=None, docker_image=None):
        """

        :param name:
        :param genpipes_wf:
        """

        self.name = name
        self.gp_wf = genpipes_wf
        self.docker_image = docker_image
        self.readset_in = readset_in


        self.samples = defaultdict(list)
        self.samples_readset = []
        self.readset = []
        for s in self.gp_wf.sample_list:
            for r in s.readsets:
                self.samples[s.name].append(r.name)
                self.samples_readset += [s.name, r.name]
                self.readset += [r.name]

        self._tasks = []
        self.populate_task(self.gp_wf, readset_in=readset_in)
        self.pick_inputs_from_readset_file()

    def pick_inputs_from_readset_file(self):
        pass

    def clean_list(self, samples, readset, input_files, rstrip=False):

        def replace_text(text, word_list, tag):
            for w in word_list:
                text = text.replace(w, tag)
            return text

        update_set = collections.OrderedDict()

        for input_text in input_files:

            if len(readset) > 3:
                # gather all readset
                new_input_file = replace_text(input_text,
                                              self.samples.keys(), self.ALL_SAMPLE)
                new_input_file = replace_text(new_input_file,
                                              self.readset, self.ALL_READSET)
                clean_type = [self.ALL_SAMPLE, self.ALL_READSET]
            elif len(readset) > 1:
                # gather on a sample readset
                new_input_file = replace_text(input_text,
                                              self.samples.keys(), self.SAME_SAMPLE)
                new_input_file = replace_text(new_input_file,
                                              self.readset, self.ALL_SAMPLE_READSET)
                clean_type = [self.SAME_SAMPLE, self.ALL_SAMPLE_READSET]
            elif len(samples) > 1:
                # gather on samples
                new_input_file = replace_text(input_text,
                                              self.samples.keys(), self.ALL_SAMPLE)
                clean_type = [self.ALL_SAMPLE]
            elif len(samples) == 1 and len(readset) == 1:
                # scatter on readset
                new_input_file = replace_text(input_text,
                                              self.samples.keys(), self.SAME_SAMPLE)
                new_input_file = replace_text(new_input_file,
                                              self.readset, self.SAME_READSET)
                clean_type = [self.SAME_SAMPLE, self.SAME_READSET]
            elif len(readset) == 1:
                # scatter on readset
                new_input_file = replace_text(input_text,
                                              self.readset, self.SAME_READSET)
                clean_type = [self.SAME_READSET]
            elif len(samples) == 1:
                # scatter on sample
                new_input_file = replace_text(input_text,
                                              self.samples.keys(), self.SAME_SAMPLE)
                clean_type = [self.SAME_SAMPLE]
            else:
                new_input_file = input_text
                clean_type = None

            do_remove = False
            if rstrip:
                new_input_file = new_input_file.rstrip()

                if '\\' in new_input_file:
                    new_input_file = new_input_file.rstrip('\\')
                    do_remove = True
                new_input_file = new_input_file.rstrip()

            if do_remove:
                update_set[new_input_file+' \\'] = None
            else:
                update_set[new_input_file] = None

        return (update_set.keys(), clean_type)

    def task_name(self, name):

        j_name_part = name.split('.')

        elem = [i for i, npart in enumerate(j_name_part)
                if npart in self.samples_readset]
        if elem:
            elem = elem[0]
            task_name = '.'.join(j_name_part[0:elem])
            extra_scatter = j_name_part[elem+1:]
            if extra_scatter:
                task_name = '{}.{}'.format(task_name, ".".join(extra_scatter))
        else:
            task_name = '.'.join(j_name_part)

        return task_name

    def populate_task(self, gp_wf, readset_in=None):
        """

        """
        for job in gp_wf.jobs:

            task_name = self.task_name(job.name)

            new_task = True
            for t in self._tasks:
                if task_name == t.name:
                    new_task = False
                    break

            if new_task:
                t = Task(task_name)
                self._tasks.append(t)

            # Recursibely readset  inputs
            # Change Workdir for "./"
            # Look for readset scatter-gather
            # Look for sample scatter-gather
            # Look for other scatter-gather


            def check_if_in_file_list(all_files):
                samples = []
                readset = []
                for s, readsets in self.samples.items():
                    if s in all_files:
                        samples.append(s)
                    for r in readsets:
                        if r in all_files:
                            readset.append(s)
                return (samples, readset)

            samples, readset = check_if_in_file_list(" ".join(job.input_files))

            (cln_input_files, i_clean_type) = self.clean_list(samples, readset, job.input_files)

            samples, readset = check_if_in_file_list(" ".join(job.output_files))

            (cln_output_files, o_clean_type) = self.clean_list(samples, readset, job.output_files)

            samples, readset = check_if_in_file_list(job.command)

            (cln_cmd, c_clean_type) = self.clean_list(samples, readset, job.command.split('\n'), rstrip=True)
            cln_cmd = '\n'.join(cln_cmd)

            t.gen_jobs.append(job)
            t.input_files = cln_input_files
            # t.input_files = job.input_files
            t.input_type = i_clean_type
            t.output_files = cln_output_files
            # t.output_files = job.output_files
            t.output_type = o_clean_type
            t.command = cln_cmd
            t.command_type = c_clean_type


    def add_task_to_wdl_str(self):
        pass





    def wdl_str(self):
        wf = (
"""
workflow {name} {{
    
}}
""".format(name=self.name))




        task_str = (
"""
task {name} {{

	command<<<
	    {command}
	>>>

	output {{
        {output}
	}}

	runtime {{
		docker: "{image}" 
	}}


}}
"""
    )
        tasks = ''
        for task in self._tasks:
            t = task_str.format(name=task.name, output=task.output_files, command=task.command,
                            image=self.docker_image)
            tasks += t

        return tasks


def readset_to_dict(readset_in, workflow_name='wf'):
    """
    :param readset_in:
    :return: {Sample1: {Readset11: {key:val},
                        Readset12: {key:val}, [...] },
              Sample2: {Readset21: {key:val}, [...]} [...] }
    """

    all_info = {}

    def the_val(rs):
        """Current queried values in the readset.
        """
        keys = ['fastq1',
                'fastq2',
                'adapter1',
                'adapter2',
                'library',
                'run_type',
                'run',
                'lane',
                'quality_offset',
                'beds',
                'bam',
                'primer1',
                'primer2',
                'name']
        bidon = {k: getattr(rs, k, None) for k in keys}
        bidon['sample'] = rs.sample.name
        return bidon

    all_info = []
    for elem in readset_in:
        if elem.sample.name not in [s['name'] for s in all_info]:
            all_info.append({'name': elem.sample.name, 'readsets': [the_val(r) for r in elem.sample.readsets]})

    return {'{}.samples'.format(workflow_name): all_info}


def make_wdl(genpipeline, readset_in):
    """

    :param genpipeline: A Genpipes Pipeline class object
    :return: A wdl scrip
    """
    if not isinstance(genpipeline, Pipeline):
        raise IOError("genpipeline, {} is not an instance of Pipeline".format(type(genpipeline)))


    workflow = Workflow(genpipeline.__class__.__name__, genpipeline, readset_in)


    print workflow.wdl_str()

def plot_graph(pipeline, readset_in=None):

    label = {}
    G = nx.DiGraph()
    all_root = {}
    pos = {}
    in_link = collections.defaultdict(list)
    order = 1
    for job in pipeline.jobs:
        name = job.name
        G.add_node(name)

        root_name = name.split('.')[0]
        if all_root.get(root_name) is None:
            rand = 1+(3*random.random()+0.5)*(order % 2)
            all_root[root_name] = [order, rand]
            label[name] = ''
            pos[name] = (order, rand)
            # name node
            label_node = 'name_node'+root_name
            G.add_node(label_node)
            label[label_node] = root_name
            pos[label_node] = (order, 0)
            order = order + 1
        else:
            all_root[root_name][1] += 1
            if all_root[root_name][1] > 20 :
                all_root[root_name][1] = 20
            pos[name] = (all_root[root_name][0], all_root[root_name][1])
            label[name] = ''

        for in_f in job.input_files:
            in_link[in_f].append(job.name)

    for job in pipeline.jobs:
        for in_f in job.output_files:
            connection_list = in_link.get(in_f)
            if connection_list:
                for edge in connection_list:
                    G.add_edge(job.name, edge)

    G.add_node('INPUTS')
    pos['INPUTS'] = (0, 0)
    label['INPUTS'] = 'INPUT'
    for readset in readset_in:
        connection_list = in_link.get(readset.bam)
        if connection_list:
            for edge in connection_list:
                G.add_edge('INPUTS', edge)

    # rs = readset_in[0]
    # for job in pipeline.jobs:
    #     if rs.library in job.command:
    #         G.add_edge('INPUTS', job.name)
    #     if rs.adapter1 in job.command:
    #         G.add_edge('INPUTS', job.name)
    #     if rs.adapter2 in job.command:
    #         G.add_edge('INPUTS', job.name)

    # nx.draw_networkx_labels(G, pos=pos, label=label)
    color_map = []
    for n in G.nodes:
        if 'name_node' in n:
            color_map.append('white')
        else:
            color_map.append('red')

    nx.draw(G, pos=pos, with_labels=False, node_color=color_map)
    # nx.draw(G, with_labels=False, node_color=color_map)
    text = nx.draw_networkx_labels(G, pos, labels=label)
    for t in text.items():
        t[1].set_rotation('vertical')

    plt.show()

if __name__ == '__main__':




    # ChipSeq(python_interface=True,config_files=['/home/poq/c3g/wdl/genpipes/pipelines/chipseq/chipseq.base.ini']
    #         , job_scheduler='batch', validate_modules=False, steps='1-3',
    #         readsets="/home/poq/container_cvmfs/data/chipseq/readsets.chipseqTest.chr22.tsv")

    # ChipSeq(validate_modules=False)

    # pipeline = RnaSeq(validate_modules=False, config_files=['/home/poq/c3g/wdl/genpipes/pipelines/rnaseq/rnaseq.base.ini'],
    #        steps='1-15', job_scheduler="batch", python_interface=True,
    #        readsets="/home/poq/container_cvmfs/data/rnaseq/C3GAW_RNA_TestData_Aug2018/readset.rnaseq_dummy.txt",
    #        design="/home/poq/container_cvmfs/data/rnaseq/C3GAW_RNA_TestData_Aug2018/design.rnaseq.txt",
    #        print_output_script=False, protocol=['cufflinks'], analyse_type='cufflinks', sanity_check=True)

    root_data = "/home/poq/c3g/data/fake"
    # rs = "bidon.readset.tsv"
    rs = 'uniq.tsv'
    # rs = 'un.tsv'
    readset_file = '{}/{}'.format(root_data, rs)
    # rs = 'un_sample.tsv'
    root_config = os.path.dirname(inspect.getfile(DnaSeq))

    pipeline = DnaSeq(python_interface=True,
                      readsets=readset_file,
                      config_files=['{}/dnaseq.base.ini'.format(root_config)],
                      # config_files=['{}/dnaseq.base.ini'.format(root_config)], steps='1-8',
                      job_scheduler='batch', validate_modules=False, protocol=['mugqic'], sanity_check=True)

    pipeline.submit_jobs()
    # RnaSeq(validate_modules=False)
    # DnaSeq(protocol=['mugqic'], validate_modules=False)
    with open('{}/two'.format(root_data), 'w') as fp:
           fp.write("\n".join(["JOB {0} \n\tInput {1} "
                               "\n\tOutput_dir {3}\n\tOutput {2} \n\t cmd   {4}"
                              .format(j.name, "\n\t Input ".join(j.input_files),
                                      "\n\tOutput ".join(j.output_files),
                                      j.output_dir,
                                      "\n\t       ".join(j.command.split('\n')))
                               for j in pipeline.jobs]))

    readset_in = genpipes.bfx.readset.parse_illumina_readset_file(readset_file)

    import json
    with open(os.path.join(root_data, rs.replace('.tsv', '.json')), 'w') as fp:
        json.dump(readset_to_dict(readset_in), fp)
    # Need to ad a value the the keys or put them in lists

    # plot_graph(pipeline, readset_in)

    make_wdl(pipeline, readset_in)
    # pipeline.submit_jobs()


